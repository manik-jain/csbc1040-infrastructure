FROM node:12.16.3-slim
COPY . ./app
WORKDIR /app
RUN npm install
RUN chmod +x wait-for-it.sh
CMD npm run start
