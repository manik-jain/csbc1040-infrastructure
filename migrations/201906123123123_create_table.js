exports.up = (knex, Promise) =>
  knex.schema.createTable('main', t => {
    t.increments('id').primary().unsigned()
    t.string('first')
    t.string('last')
    t.string('email')
    t.string('phone')
    t.string('location')
    t.string('hobby')
    t.unique('email')
    t.timestamps(false, true)
  })

exports.down = (knex, Promise) => knex.schema.dropTable('main')
