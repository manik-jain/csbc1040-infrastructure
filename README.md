# CSBC1040 Infrastructure

The project relies on CI/CD pipelines to deploy the latest changes to Google cloud.
Any changes merged/pushed to master branch will trigger a Continuous Deployment pipeline to deploy the most recent changes to the cloud.

The project has 3 pieplines : 

1. Automated test cases : this checks if all the test cases are working fine or not
2. Build a docker image : this relies on the Docker-compose to create a docker image
3. Deploy docker image : this deploys the docker image to the cloud.

Please check the .gitlab.yml file for detailed pipeline stages.
